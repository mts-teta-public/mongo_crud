package main

import (
	"context"
	"log"
	"time"

	"mongo_crud/pkg/observability"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opentelemetry.io/contrib/instrumentation/go.mongodb.org/mongo-driver/mongo/otelmongo"
)

const URI = "mongodb://91.185.93.83:27017/team0"

func getClient(ctx context.Context) (*mongo.Client, error) {
	ctx, span := observability.FollowSpan(ctx, "getClient")
	defer span.End()

	opts := options.Client()
	opts.ApplyURI(URI)
	optsAuth := options.Credential{
		Username:   "team0",
		Password:   "mNgdaATbhVGd",
		AuthSource: "team0",
	}

	opts.SetAuth(optsAuth)

	opts.Monitor = otelmongo.NewMonitor()

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	tc, err := observability.New(observability.Config{
		ServiceName: "mongo_example",
		TracerName:  "mongo_example",
		Debug:       false,
	})
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		errTCStop := tc.Stop(ctx)
		if errTCStop != nil {
			log.Fatal(errTCStop)
		}
	}()

	// =========
	// ==START==
	// =========
	ctx, span := observability.FollowSpan(ctx, "main")

	// Создаём клиента
	client, err := getClient(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	defer func() {
		if mongoDisconectErr := client.Disconnect(ctx); mongoDisconectErr != nil {
			log.Fatal(mongoDisconectErr)
		}
	}()

	// Все примеры будут для одной колллекции поэтому сразу создаём соответствующий объект
	col := client.Database("team0").Collection("books")

	// Чтобы точно начать с чистого листа удалим коллекцию вообще.
	err = col.Drop(ctx)
	if err != nil {
		log.Fatal(err)
	}

	insertOne(ctx, col)
	insertMany(ctx, col)
	findAll(ctx, col)
	find(ctx, col)
	findWithCondition(ctx, col)
	findWithOrCondition(ctx, col)
	updateOne(ctx, col)
	updateMany(ctx, col)
	replaceOne(ctx, col)
	deleteAllRows(ctx, col)

	span.End()
	tc.TracerProvider.ForceFlush(ctx)
	_ = tc.TracerProvider.Shutdown(ctx)
	_ = tc.Exporter.Shutdown(ctx)
}
