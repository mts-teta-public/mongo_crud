package observability

import (
	"context"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func SetError(span trace.Span, err error) {
	span.SetStatus(codes.Error, err.Error())
}

func SetComponentName(span trace.Span, component string) {
	atr := attribute.String("component", component)
	if atr.Valid() {
		span.SetAttributes(atr)
	}
}

func FollowSpan(ctx context.Context, name string) (_ context.Context, span trace.Span) {
	ctx, span = tracer.Start(ctx, name)
	return ctx, span
}
