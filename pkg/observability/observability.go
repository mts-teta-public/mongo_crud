package observability

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	stdout "go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
)

var tracer trace.Tracer

type Config struct {
	JaegerUrl   string `yaml:"jaeger_url"`
	Debug       bool   `yaml:"debug"`
	ServiceName string `yaml:"service_name"`
	TracerName  string `yaml:"tracer_name"`
}

type TracingContainer struct {
	*otlptrace.Exporter
	*tracesdk.TracerProvider
}

func New(cfg Config) (*TracingContainer, error) {
	exp, err := otlptracehttp.New(context.Background(), otlptracehttp.WithInsecure())
	if err != nil {
		return nil, err
	}

	var tp *tracesdk.TracerProvider
	tpOptions := []tracesdk.TracerProviderOption{
		tracesdk.WithBatcher(exp),
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(cfg.ServiceName),
		)),
	}

	if cfg.Debug {
		stdoutexp, err := stdout.New(stdout.WithPrettyPrint())
		if err != nil {
			return nil, err
		}
		tpOptions = append(tpOptions,
			tracesdk.WithBatcher(stdoutexp),
			tracesdk.WithSampler(tracesdk.AlwaysSample()),
		)
	}

	tp = tracesdk.NewTracerProvider(tpOptions...)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.TraceContext{})

	tracer = otel.Tracer(cfg.TracerName)

	return &TracingContainer{
		Exporter:       exp,
		TracerProvider: tp,
	}, nil
}

func (tc *TracingContainer) Stop(ctx context.Context) error {
	err := tc.ForceFlush(ctx)
	if err != nil {
		return fmt.Errorf("OTEL exporter flush failed: %w", err)
	}

	err = tc.Exporter.Shutdown(ctx)
	if err != nil {
		return fmt.Errorf("OTEL exporter shutdown failed: %w", err)
	}

	err = tc.TracerProvider.Shutdown(ctx)
	if err != nil {
		return fmt.Errorf("tracer provider shutdown failed: %w", err)
	}
	return nil
}
